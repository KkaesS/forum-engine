## Forum engine

### Description
---
Spring REST API which allows you to create posts and comments.
#### Api documentation
`http://localhost:8080/swagger-ui.html`


### Technologies
---
* Spring Boot
* Spring REST
* Spring HATEOAS
* Spring Security
* Spring Data MongoDB
* OAuth 2.0
* Project Lombok
* JUnit 5
* Mockito
* Swagger 2

### Requirements
---
For building and running the application you need:

* JDK 12
* Maven 3
* MongoDB

### Build Tool
---
* Maven (3.6.1)

### Run with Maven
---

```
git clone https://KkaesS@bitbucket.org/KkaesS/forum-engine.git
cd forum-engine
mvn spring-boot:run
```

### Testing
---
#### Generate a token
`curl -u LXNE3EHYKNXOHWQXSR2IB1F66ZMKQB0E:NcRfUjXn2r5u8x/A%D*G-KaPdSgVkYp3 -X POST http://localhost:8080/oauth/token -H "Content-Type: application/x-www-form-urlencoded" -d "username=donald&password=password&grant_type=password"`  

#### Save Post
`curl -X POST -d '{"id":"1","title":"Post title","body":"body of post"}' http://localhost:8080/api/v1/posts -H "Content-Type: application/json" -H "Authorization: Bearer <Token>"`

#### Save Comment
`curl -X POST -d '{"body":"body of comment"}' http://localhost:8080/api/v1/comments/posts/1 -H "Content-Type: application/json" -H "Authorization: Bearer <Token>"`

#### Get all posts
`curl http://localhost:8080/api/v1/posts`