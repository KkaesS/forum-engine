package pl.ks.forumengine.posts;

import io.swagger.annotations.*;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ks.forumengine.exception.Error;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by ks on 13.08.2019
 */

@Api(tags = "posts")
@RestController
@RequestMapping("/api/v1")
public interface PostApi {

    @ApiOperation("Find all posts")
    @ApiResponse(code = 200, message = "Posts returned successfully", response = PostResource.class)
    @GetMapping(value = "/posts", produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    Resources<PostResource> findAllPost();

    @ApiOperation("Find the post by the given ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post returned successfully", response = PostResource.class),
            @ApiResponse(code = 404, message = "Post not found", response = Error.class)
    })
    @GetMapping(value = "/posts/{id}", produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PostResource findPostById(@ApiParam(value = "ID of post to return", required = true) @PathVariable String id);

    @ApiOperation("Save the post")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Post saved successfully"),
            @ApiResponse(code = 400, message = "Input validation field", response = Error.class)
    })
    @PostMapping(value = "/posts", produces = "application/json")
    ResponseEntity<PostDto> savePost(Principal user, @ApiParam(value = "Post object that will be saved", required = true) @Valid @RequestBody PostDto postDto);

    @ApiOperation("Delete the post by the given ID")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Post removed successfully"),
            @ApiResponse(code = 404, message = "Post not found", response = Error.class)
    })
    @DeleteMapping(value = "/posts/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deletePostById(@ApiParam(value = "ID of post to delete", required = true) @PathVariable String id);
}
