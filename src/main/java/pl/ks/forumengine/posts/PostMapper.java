package pl.ks.forumengine.posts;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.ks.forumengine.comments.CommentMapper;

/**
 * Created by ks on 14.08.2019
 */

@Mapper(uses = CommentMapper.class)
public interface PostMapper {

    @Mapping(target = "_id", source = "id")
    Post mapToPost(PostDto postDto);

    @InheritInverseConfiguration
    @Mapping(target = "id", source = "_id")
    PostDto mapToPostDto(Post post);
}
