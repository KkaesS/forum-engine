package pl.ks.forumengine.posts;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.ks.forumengine.comments.Comment;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ks on 13.08.2019
 */

@Data
@Builder
@Document(collection = "posts")
class Post {

    @Id
    private String _id;

    private String username;

    private String title;

    private String body;

    private LocalDateTime createdAt;

    private List<Comment> comments;
}
