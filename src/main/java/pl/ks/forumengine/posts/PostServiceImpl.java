package pl.ks.forumengine.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ks.forumengine.comments.CommentMapper;
import pl.ks.forumengine.exception.ResourceNotFoundException;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ks on 14.08.2019
 */

@Service
class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;

    @Autowired
    PostServiceImpl(PostRepository postRepository, PostMapper postMapper, CommentMapper commentMapper) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
    }

    @Override
    public PostDto save(PostDto postToSave, String user) {
        postToSave.setUsername(user);
        Post saved = postRepository.save(postMapper.mapToPost(postToSave));
        return postMapper.mapToPostDto(saved);
    }

    @Override
    public PostDto findById(String id) {
        return postRepository.findById(id)
                .map(postMapper::mapToPostDto)
                .orElseThrow(() -> new ResourceNotFoundException("Post with id: " + id + " not found"));
    }

    @Override
    public List<PostDto> findAll() {
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapToPostDto)
                .collect(Collectors.toList());
    }

    @Override
    public void update(PostDto updatedPostDto) {
        postRepository.findById(updatedPostDto.getId())
                .map(post -> {
                    if (updatedPostDto.getTitle() != null)
                        post.setTitle(updatedPostDto.getTitle());

                    if (updatedPostDto.getBody() != null)
                        post.setBody(updatedPostDto.getBody());

                    if (updatedPostDto.getComments() != null) {
                        post.setComments(updatedPostDto.getComments().stream()
                                .map(commentMapper::mapToComment)
                                .collect(Collectors.toList()));
                    }
                    return postMapper.mapToPostDto(postRepository.save(post));
                });
    }

    @Override
    public void delete(String id) {
        Post postToDelete = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post with id: " + id + " not found"));
        postRepository.delete(postToDelete);
    }

    @Override
    public void deleteAll() {
        postRepository.deleteAll();
    }
}
