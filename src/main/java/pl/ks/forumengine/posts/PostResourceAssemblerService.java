package pl.ks.forumengine.posts;

import org.springframework.hateoas.Resources;

import java.util.List;

/**
 * Created by ks on 17.08.2019
 */
public interface PostResourceAssemblerService {
    Resources<PostResource> toResources(List<PostDto> list);

    PostResource toResource(PostDto dto);
}
