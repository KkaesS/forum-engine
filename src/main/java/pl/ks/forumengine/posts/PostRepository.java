package pl.ks.forumengine.posts;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ks on 13.08.2019
 */
interface PostRepository extends MongoRepository<Post, String> {
}
