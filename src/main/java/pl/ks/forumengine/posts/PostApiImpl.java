package pl.ks.forumengine.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.List;

/**
 * Created by ks on 13.08.2019
 */
@Component
class PostApiImpl implements PostApi {

    private final PostService postService;
    private final PostResourceAssemblerService postResourceAssemblerService;

    @Autowired
    PostApiImpl(PostService postService, PostResourceAssemblerService postResourceAssemblerService) {
        this.postService = postService;
        this.postResourceAssemblerService = postResourceAssemblerService;
    }

    @Override
    public Resources<PostResource> findAllPost() {
        List<PostDto> postDtoList = postService.findAll();
        return postResourceAssemblerService.toResources(postDtoList);
    }


    @Override
    public PostResource findPostById(String id) {
        PostDto foundPostDto = postService.findById(id);
        return postResourceAssemblerService.toResource(foundPostDto);
    }

    @Override
    public ResponseEntity<PostDto> savePost(Principal user, PostDto postDto) {
        PostDto postToSave = postService.save(postDto, user.getName());

        HttpHeaders httpHeaders = new HttpHeaders();
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(postToSave.getId())
                .toUri();
        httpHeaders.setLocation(uri);

        return new ResponseEntity<>(postToSave, httpHeaders, HttpStatus.CREATED);

    }

    @Override
    public void deletePostById(String id) {
        postService.delete(id);
    }
}
