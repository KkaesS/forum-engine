package pl.ks.forumengine.posts;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.*;
import pl.ks.forumengine.comments.CommentDto;
import pl.ks.forumengine.security.User;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ks on 13.08.2019
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Post")
public class PostDto {

    private String id;

    private String username;

    @NotNull
    private String title;

    @NotNull
    private String body;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime createdAt = LocalDateTime.now();

    private List<CommentDto> comments;
}
