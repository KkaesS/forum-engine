package pl.ks.forumengine.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.ks.forumengine.security.User;
import pl.ks.forumengine.security.UserRepository;

import java.time.LocalDateTime;

/**
 * Created by ks on 14.08.2019
 */

@Component
class Bootstrap implements CommandLineRunner {

    private final PostService postService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    Bootstrap(PostService postService, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.postService = postService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {

        postService.deleteAll();
        userRepository.deleteAll();




        User user = User.builder()
                .firstName("Donald")
                .lastName("Duck")
                .password(passwordEncoder.encode("password"))
                .username("donald")
                .isAdmin(false)
                .build();

        User admin = User.builder()
                .firstName("Mickey")
                .lastName("Mouse")
                .password(passwordEncoder.encode("password"))
                .username("mickey")
                .isAdmin(true)
                .build();

        userRepository.save(user);
        userRepository.save(admin);

        PostDto first = PostDto.builder()
                .title("Hello everyone")
                .body("My first post")
                .createdAt(LocalDateTime.now())
                .build();
        postService.save(first, user.getUsername());

        PostDto second = PostDto.builder()
                .title("I have a problem")
                .body("My computer won't turn on")
                .createdAt(LocalDateTime.now())
                .build();

        postService.save(second, admin.getUsername());
    }
}
