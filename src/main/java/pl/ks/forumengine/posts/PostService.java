package pl.ks.forumengine.posts;

import java.util.List;

/**
 * Created by ks on 14.08.2019
 */
public interface PostService {

    PostDto save(PostDto postToSave, String user);

    PostDto findById(String id);

    List<PostDto> findAll();

    void update(PostDto updatedPost);

    void delete(String id);

    void deleteAll();
}
