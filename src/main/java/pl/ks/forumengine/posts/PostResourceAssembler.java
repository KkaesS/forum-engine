package pl.ks.forumengine.posts;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * Created by ks on 16.08.2019
 */
@Component
public class PostResourceAssembler extends ResourceAssemblerSupport<PostDto, PostResource> {

    PostResourceAssembler() {
        super(PostApi.class, PostResource.class);
    }

    @Override
    protected PostResource instantiateResource(PostDto postDto) {
        return new PostResource(postDto);
    }


    @Override
    public PostResource toResource(PostDto postDto) {
        if (postDto.getComments() == null) {
            postDto.setComments(Collections.emptyList());
        }
        return createResourceWithId("posts/" + postDto.getId(), postDto);
    }
}
