package pl.ks.forumengine.posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ks on 17.08.2019
 */
@Service
class PostResourceAssemblerServiceImpl implements PostResourceAssemblerService {

    private final PostResourceAssembler postResourceAssembler;

    @Autowired
    PostResourceAssemblerServiceImpl(PostResourceAssembler postResourceAssembler) {
        this.postResourceAssembler = postResourceAssembler;
    }

    @Override
    public Resources<PostResource> toResources(List<PostDto> list) {
        List<PostResource> postResources = postResourceAssembler.toResources(list);
        Resources<PostResource> resources = new Resources<>(postResources);

        resources.add(
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostApi.class)
                        .findAllPost())
                        .withSelfRel());

        return resources;

    }

    @Override
    public PostResource toResource(PostDto dto) {
        return postResourceAssembler.toResource(dto);
    }
}
