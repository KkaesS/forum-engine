package pl.ks.forumengine.posts;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;
import pl.ks.forumengine.comments.CommentResource;
import pl.ks.forumengine.comments.CommentResourceAssembler;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * Created by ks on 16.08.2019
 */

@EqualsAndHashCode(callSuper = false)
@Relation(value = "post", collectionRelation = "posts")
public class PostResource extends ResourceSupport {

    private static final CommentResourceAssembler COMMENT_RESOURCE_ASSEMBLER = new CommentResourceAssembler();

    @Getter
    private final String title;

    @Getter
    private String username;

    @Getter
    private final String body;

    @Getter
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private final LocalDateTime createdAt;

    @Getter
    private final List<CommentResource> comments;

    PostResource(final PostDto postDto) {
        this.title = postDto.getTitle();
        this.body = postDto.getBody();
        this.username = postDto.getUsername();
        this.createdAt = postDto.getCreatedAt();
        if (postDto.getComments() == null)
            postDto.setComments(Collections.emptyList());

        this.comments = COMMENT_RESOURCE_ASSEMBLER.toResources(postDto.getComments());

    }
}