package pl.ks.forumengine.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import pl.ks.forumengine.comments.CommentResource;
import pl.ks.forumengine.posts.PostResource;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by ks on 27.08.2019
 */

@EnableSwagger2
@Configuration
class SwaggerConfig {

    @Bean
    public Docket swaggerApi() {
        Class[] ignoreClasses = {CommentResource.class, Link.class, PostResource.class, Resources.class};

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .ignoredParameterTypes(ignoreClasses)
                .tags(new Tag("posts", "All posts operations"),
                        new Tag("comments", "All comments operations"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("pl.ks.forumengine"))
                .paths(PathSelectors.regex("/api.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Forum Engine API")
                .description("Create posts and comments")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(new Contact("Konrad", "https://bitbucket.org/KkaesS/", "konrad.surmacz15@gmail.com"))
                .build();
    }
}