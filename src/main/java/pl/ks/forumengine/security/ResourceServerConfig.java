package pl.ks.forumengine.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Created by ks on 20.08.2019
 */
@Configuration
@EnableResourceServer
class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String SECURED_POSTS = "/api/v1/posts/**";
    private static final String SECURED_COMMENTS = "/api/v1/comments/**";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("api");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_POSTS).hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, SECURED_POSTS).hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, SECURED_COMMENTS).hasRole("USER")
                .antMatchers(HttpMethod.DELETE, SECURED_COMMENTS).hasRole("USER")
                .anyRequest().permitAll();

    }
}
