package pl.ks.forumengine.security;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ks on 20.08.2019
 */

@Document
@Data
@Builder
public class User {

    @Id
    private String id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private boolean isAdmin;
}
