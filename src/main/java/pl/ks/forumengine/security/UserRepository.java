package pl.ks.forumengine.security;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ks on 20.08.2019
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(String username);
}
