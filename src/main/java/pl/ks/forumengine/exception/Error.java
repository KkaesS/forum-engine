package pl.ks.forumengine.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ks on 19.08.2019
 */
@Getter
@Setter
public class Error {

    private String title;
    private int status;
    private String detail;
    private long timeStamp;
    private String developerMessage;

    private Map<String, List<ValidationError>> errors = new HashMap<>();


    @Getter
    @Setter
    static class ValidationError {
        private String code;
        private String message;
    }
}