package pl.ks.forumengine.comments;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ks.forumengine.exception.Error;

import javax.validation.Valid;

/**
 * Created by ks on 14.08.2019
 */

@Api(tags = "comments")
@RestController
@RequestMapping("/api/v1")
public interface CommentApi {

    @ApiOperation("Find the comment by the given ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comment returned successfully", response = CommentDto.class),
            @ApiResponse(code = 404, message = "Comment not found", response = Error.class)
    })
    @GetMapping(value = "/comments/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    CommentDto findById(@ApiParam(value = "ID of comment to return", required = true) @PathVariable String id);

    @ApiOperation("Save the comment")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Comment saved successfully"),
            @ApiResponse(code = 400, message = "Input validation field", response = Error.class)
    })
    @PostMapping(value = "/comments/posts/{id}", produces = "application/json")
    ResponseEntity<CommentDto> save(@ApiParam(name = "comment", value = "Comment object that will be saved", required = true) @Valid @RequestBody CommentDto commentDto,
                                    @ApiParam(value = "ID of post to comment", required = true) @PathVariable("id") String postId);

    @ApiOperation("Delete the comment by the given ID")
    @DeleteMapping(value = "/comments/{id}", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Comment removed successfully"),
            @ApiResponse(code = 404, message = "Comment not found", response = Error.class)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@ApiParam(value = "ID of comment to delete", required = true) @PathVariable String id);
}
