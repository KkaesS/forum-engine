package pl.ks.forumengine.comments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * Created by ks on 14.08.2019
 */

@Component
@Slf4j
class CommentApiImpl implements CommentApi {

    private final CommentService commentService;

    @Autowired
    CommentApiImpl(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public CommentDto findById(String id) {
        return commentService.findById(id);
    }

    @Override
    public ResponseEntity<CommentDto> save(CommentDto commentDto, String postId) {
        CommentDto savedComment = commentService.save(commentDto, postId);
        log.info("save " + savedComment.getId());

        HttpHeaders httpHeaders = new HttpHeaders();
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedComment.getId())
                .toUri();
        httpHeaders.setLocation(uri);

        return new ResponseEntity<>(savedComment, httpHeaders, HttpStatus.CREATED);
    }

    @Override
    public void delete(String id) {
        commentService.deleteById(id);
    }
}
