package pl.ks.forumengine.comments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by ks on 16.08.2019
 */
@Slf4j
public class CommentResourceAssembler extends ResourceAssemblerSupport<CommentDto, CommentResource> {

    public CommentResourceAssembler() {
        super(CommentApi.class, CommentResource.class);
    }

    @Override
    protected CommentResource instantiateResource(CommentDto entity) {
        log.info("instantiateResource: " + entity.getId());
        return new CommentResource(entity);
    }

    @Override
    public CommentResource toResource(CommentDto commentDto) {
        log.info("toResource: " + commentDto.getId());
        return createResourceWithId("comments/" + commentDto.getId(), commentDto);
    }
}
