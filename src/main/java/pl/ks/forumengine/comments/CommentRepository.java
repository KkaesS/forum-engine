package pl.ks.forumengine.comments;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ks on 13.08.2019
 */

// TODO remove public later
public interface CommentRepository extends MongoRepository<Comment, String> {
}
