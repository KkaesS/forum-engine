package pl.ks.forumengine.comments;

/**
 * Created by ks on 14.08.2019
 */
public interface CommentService {

    CommentDto findById(String id);

    CommentDto save(CommentDto commentDto, String postId);

    void deleteById(String id);
}
