package pl.ks.forumengine.comments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ks.forumengine.exception.ResourceNotFoundException;
import pl.ks.forumengine.posts.PostDto;
import pl.ks.forumengine.posts.PostService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ks on 14.08.2019
 */
@Service
@Slf4j
class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final PostService postService;
    private final CommentMapper commentMapper;

    @Autowired
    CommentServiceImpl(CommentRepository commentRepository, PostService postService
            , CommentMapper commentMapper) {
        this.commentRepository = commentRepository;
        this.postService = postService;
        this.commentMapper = commentMapper;
    }

    @Override
    public CommentDto findById(String id) {
        return commentRepository.findById(id)
                .map(commentMapper::mapToCommentDto)
                .orElseThrow(() -> new ResourceNotFoundException("Comment with id: " + id +" not found"));
    }

    @Override
    public CommentDto save(CommentDto commentDto, String postId) {
        Comment savedComment = commentRepository.save(commentMapper.mapToComment(commentDto));
        PostDto postDto = postService.findById(postId);

        if (postDto.getComments() != null)
            postDto.getComments().add(commentMapper.mapToCommentDto(savedComment));

        else {
            List<CommentDto> commentDtoList = new ArrayList<>();
            commentDtoList.add(commentMapper.mapToCommentDto(savedComment));
            postDto.setComments(commentDtoList);
        }

        postService.update(postDto);
        log.info("save in service " + savedComment.get_id());

        return commentMapper.mapToCommentDto(savedComment);
    }

    @Override
    public void deleteById(String id) {
        Comment commentToDelete = commentRepository.findById(id)
                .orElseThrow(() ->  new ResourceNotFoundException("Comment with id: " + id +" not found"));

        commentRepository.delete(commentToDelete);
    }
}