package pl.ks.forumengine.comments;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Created by ks on 14.08.2019
 */
@Mapper
public interface CommentMapper {

    @Mapping(target = "_id", source = "id")
    Comment mapToComment(CommentDto commentDto);

    @InheritInverseConfiguration
    @Mapping(target = "id", source = "_id")
    CommentDto mapToCommentDto(Comment comment);
}
