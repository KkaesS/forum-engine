package pl.ks.forumengine.comments;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Created by ks on 13.08.2019
 */

@Data
@Builder
@Document(collection = "comments")
public class Comment {

    @Id
    private String _id;

    private String body;

    private LocalDateTime createdAt;
}