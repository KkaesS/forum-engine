package pl.ks.forumengine.comments;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import java.time.LocalDateTime;

/**
 * Created by ks on 16.08.2019
 */
@EqualsAndHashCode(callSuper = false)
@Relation(value = "comment", collectionRelation = "comments")
public class CommentResource extends ResourceSupport {

    @Getter
    private final String body;

    @Getter
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private final LocalDateTime createdAt;

    CommentResource(final CommentDto commentDto) {
        this.body = commentDto.getBody();
        this.createdAt = commentDto.getCreatedAt();
    }

}
