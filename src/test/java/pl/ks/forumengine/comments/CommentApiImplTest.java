package pl.ks.forumengine.comments;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.ks.forumengine.posts.PostDto;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ks on 02.09.2019
 */

@ExtendWith(MockitoExtension.class)
class CommentApiImplTest {

    private static final String API_URL = "/api/v1/comments/";

    @Mock
    private CommentService commentService;

    @InjectMocks
    private CommentApiImpl commentApi;

    private MockMvc mockMvc;

    private CommentDto expectedComment;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(commentApi).build();

        expectedComment = CommentDto.builder()
                .id("1")
                .body("comment")
                .build();
    }

    @Test
    void shouldFindCommentById() throws Exception {
        when(commentService.findById(anyString())).thenReturn(expectedComment);

        mockMvc.perform(get(API_URL + "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.body", is(expectedComment.getBody())))
                .andExpect(jsonPath("$.id", is(expectedComment.getId())));

    }

    @Test
    void shouldSaveComment() throws Exception {
        when(commentService.save(any(CommentDto.class), anyString())).thenReturn(expectedComment);

        mockMvc.perform(post(API_URL + "/posts/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedComment)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.body", is(expectedComment.getBody())))
                .andExpect(jsonPath("$.id", is(expectedComment.getId())));
    }

    @Test
    void delete() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.delete(API_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedComment)))
                .andExpect(status().isNoContent());
    }
}