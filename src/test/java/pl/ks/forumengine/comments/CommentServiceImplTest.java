package pl.ks.forumengine.comments;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.ks.forumengine.exception.ResourceNotFoundException;
import pl.ks.forumengine.posts.PostDto;
import pl.ks.forumengine.posts.PostService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


/**
 * Created by ks on 31.08.2019
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CommentServiceImplTest {

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private PostService postService;

    @Spy
    private CommentMapperImpl commentMapper;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Comment expectedComment;

    private PostDto expectedPost;

    @BeforeEach
    void setUp() {
        expectedComment = Comment.builder()
                ._id("1")
                .body("comment")
                .build();

        expectedPost = PostDto.builder()
                .id("1")
                .body("Test")
                .title("Post Test")
                .build();
    }

    @Test
    void shouldFindCommentById() {
        Optional<Comment> comment = Optional.of(expectedComment);

        when(commentRepository.findById(anyString())).thenReturn(comment);

        CommentDto actualComment = commentService.findById("1");

        assertEquals(expectedComment.getBody(), actualComment.getBody());
        assertEquals("1", actualComment.getId());
    }

    @Test
    void shouldNotFindCommentById() {
        when(commentRepository.findById("1")).thenReturn(Optional.of(expectedComment));
        assertThrows(ResourceNotFoundException.class, () -> commentService.findById("34"));
    }

    @Test
    void shouldSaveCommentAndAssignToPost() {

        when(commentRepository.save(any(Comment.class))).thenReturn(expectedComment);
        when(postService.findById(anyString())).thenReturn(expectedPost);

        CommentDto savedComment = commentService.save(commentMapper.mapToCommentDto(expectedComment), anyString());

        assertEquals(expectedComment.getBody(), savedComment.getBody());
        assertEquals(expectedComment.getBody(), expectedPost.getComments().get(0).getBody());
        assertEquals(1, expectedPost.getComments().size());
    }

    @Test
    void shouldDeleteCommentById() {

        when(commentRepository.findById(anyString())).thenReturn(Optional.of(expectedComment));

        commentService.deleteById(anyString());

        verify(commentRepository, times(1)).findById(anyString());
        verify(commentRepository, times(1)).delete(any(Comment.class));

    }
}