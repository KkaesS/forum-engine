package pl.ks.forumengine.posts;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.Principal;
import java.time.LocalDateTime;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ks on 01.09.2019
 */

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PostApiImplTest {

    private static final String API_URL = "/api/v1/posts/";

    @Mock
    private PostService postService;

    @Mock
    private PostResourceAssemblerService postResourceAssemblerService;

    @InjectMocks
    private PostApiImpl postApi;

    @Mock
    private Principal principal;

    private MockMvc mockMvc;

    private PostDto expectedPost;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(postApi).build();

        expectedPost = PostDto.builder()
                .id("1")
                .title("Hello everyone")
                .username("user")
                .body("My first post")
                .build();
    }

    @Test
    void shouldFindAllPost() throws Exception {
        mockMvc.perform(get(API_URL)
                .contentType(MediaTypes.HAL_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    void shouldFindPostById() throws Exception {

        PostResource actualPost = new PostResource(expectedPost);
        when(postApi.findPostById(anyString())).thenReturn(actualPost);

        mockMvc.perform(get(API_URL + "1")
                .contentType(MediaTypes.HAL_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(actualPost.getTitle())))
                .andExpect(jsonPath("$.body", is(actualPost.getBody())));
    }

    @Test
    void shouldSavePost() throws Exception {
        String username = "user";
        when(principal.getName()).thenReturn(username);
        when(postService.save(any(PostDto.class), anyString())).thenReturn(expectedPost);

        mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedPost)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title", is("Hello everyone")))
                .andExpect(jsonPath("$.username", is("user")));
    }

    @Test
    void shouldDeletePostById() throws Exception {
        mockMvc.perform(delete(API_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(expectedPost)))
                .andExpect(status().isNoContent());
    }

}