package pl.ks.forumengine.posts;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.ks.forumengine.comments.CommentMapperImpl;
import pl.ks.forumengine.exception.ResourceNotFoundException;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by ks on 31.08.2019
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PostServiceImplTest {

    @Mock
    private PostRepository postRepository;

    @Spy
    private PostMapperImpl postMapper;

    @Spy
    private CommentMapperImpl commentMapper;

    @InjectMocks
    private PostServiceImpl postService;

    private Post post;

    @BeforeEach
    void setUp() throws Exception {
        post = Post.builder()
                ._id("1")
                .body("Test")
                .title("Post Test")
                .build();
    }

    @Test
    void shouldSavePost() {
        when(postRepository.save(any(Post.class))).thenReturn(post);
        PostDto saved = postService.save(postMapper.mapToPostDto(post),anyString());

        assertEquals(post.getTitle(), saved.getTitle());
        assertEquals("Test", saved.getBody());
    }

    @Test
    void shouldFindPostById() {
        when(postRepository.findById(anyString())).thenReturn(Optional.ofNullable(post));

        PostDto postById = postService.findById("1");

        assertEquals(post.get_id(), postById.getId());
    }

    @Test
    void shouldNotFindPostById() {
        when(postRepository.findById("1")).thenReturn(Optional.of(post));
        assertThrows(ResourceNotFoundException.class, () -> postService.findById("34"));
    }

    @Test
    void shouldFindAllPosts() {
        Post first = Post.builder()
                .title("Hello everyone")
                .body("My first post")
                .createdAt(LocalDateTime.now())
                .build();

        Post second = Post.builder()
                .title("I have a problem")
                .body("My computer won't turn on")
                .createdAt(LocalDateTime.now())
                .build();

        when(postRepository.findAll()).thenReturn(Arrays.asList(first, second));

        List<PostDto> allPosts = postService.findAll();

        assertEquals(2, allPosts.size());
    }

    @Test
    void shouldDeletePost() {
        when(postRepository.findById("1")).thenReturn(Optional.of(post));
        postService.delete("1");

        verify(postRepository, times(1)).findById("1");
        verify(postRepository, times(1)).delete(any(Post.class));
    }

    @Test
    void shouldDeleteAllPosts() {
        postService.deleteAll();
        verify(postRepository, times(1)).deleteAll();
    }
}